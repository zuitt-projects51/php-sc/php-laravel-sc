@extends('layouts.app');

@section('content')
    @if(!Auth::guest())
        @if(Auth::user()->id === $post->user_id)
            <h1>Edit Post</h1>

            <form action="{{ action('PostController@update', [$post->id]) }}" method="POST">
                @csrf
                
                <input type="hidden" name="_method" value="PUT">

                <div class="form-group">
                    <label for="title">Title</label>
                    <input id="title-input" type="text" name="title" class="form-control" value="{{$post->title}}">
                </div>
                <div class="form-group">
                    <label for="body-input">Body</label>
                    <textarea id="body-input" type="text" name="body" class="form-control" rows="5">{{$post->body}}</textarea>
                </div>
                <button type="submit" class="btn btn-primary">Submit</button>
            </form>
        @else
            <p>You cannot edit another user's post</p>
            <a href="/posts"> Go back </a>
        @endif
    @endif
@endsection