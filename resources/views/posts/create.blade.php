@extends('layouts.app');

@section('content')
    <h1>Create post</h1>
  
    <form action="{{ action('PostController@store')}}" method="POST">
        @csrf
        <div class="form-group">
            <label for="title">Title</label>
            <input id="title-input" type="text" name="title" class="form-control" placeholder="title">
        </div>
        <div class="form-group">
            <label for="body-input">Body</label>
            <textarea id="body-input" type="text" name="body" class="form-control" placeholder="Body" rows="5"></textarea>
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>

@endsection;